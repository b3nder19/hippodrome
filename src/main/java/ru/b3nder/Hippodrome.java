package ru.b3nder;

import java.util.ArrayList;
import java.util.List;

public class Hippodrome {
    private final List<Horse> horses;
    public static Hippodrome game;

    public Hippodrome() {
        horses = new ArrayList<>();
    }

    public List<Horse> getHorses() {
        return horses;
    }

    public void move() {
        for (Horse horse:
             getHorses()) {
            horse.move();
        }
    }

    public void print() {
        for (Horse horse :
                getHorses()) {
            horse.print();
        }
        System.out.println();
        System.out.println();
    }

    public void run() throws InterruptedException {
        for (int i = 1; i < 100; i++) {
            move();
            print();
            Thread.sleep(500);
        }
    }

    public Horse getWinner() {
        double max = 0;
        Horse winner = null;

        for (Horse horse :
                horses) {
            if (horse.getDistance() > max) {
                max = horse.getDistance();
                winner = horse;
            }
        }
        return winner;
    }

    public void printWinner() {
        System.out.println("Winner is " + getWinner().getName());
    }

    public static void main(String[] args) throws InterruptedException {
        game = new Hippodrome();
        Horse horse1 = new Horse("Sleven", 3, 0);
        Horse horse2 = new Horse("Lucky", 3, 0);
        Horse horse3 = new Horse("Gomer", 3, 0);

        game.horses.add(horse1);
        game.horses.add(horse2);
        game.horses.add(horse3);

        game.run();
        game.printWinner();
    }
}
